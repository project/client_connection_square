Client Connection: Square
=========================

A [Client Connection](https://www.drupal.org/project/client_connection) implementation to connect to Square's API.